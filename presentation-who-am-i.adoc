[background-image="intro.png",background-size=cover,background-opacity="1"]
== Who Am I?

* Olivier Jourdan
* 20+ years programing in {lv}
* Founder of Wovalab in 2018
* Board member of the DQMH Consortium

icon:linkedin-square[2x] https://www.linkedin.com/in/jourdanolivier/

icon:twitter-square[2x] https://twitter.com/OJourdan

[.notes]
--

--